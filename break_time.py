import time
import webbrowser

total_breaks = 3
break_count = 0

# a message that tells us when the programm begins
print ("This program started on "+time.ctime())
while (break_count < total_breaks):
# the programm runs 3 times every 10 seconds.
# 10 seconds should be changed to time when you need to make a break
# if we want to take a break every 2 hours after working on computer (2*60*60)
    time.sleep(10)
    webbrowser.open("https://www.youtube.com/watch?v=2azweZqgbyo")
    break_count = break_count + 1
# to make the loop stop press Ctrl+C